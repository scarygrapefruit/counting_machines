import React from 'react';

const DisplayCount = ({ count }) => {
  let display = '1';
  for (let idx = 2; idx <= count; idx++) {
    display += ` ${idx}`;
  }

  return (
    <div>
      <span>{ display }</span>
    </div>
  );
}

export default DisplayCount;
