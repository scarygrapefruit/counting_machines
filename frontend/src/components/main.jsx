import Minute from './machines/minute';

import '../css/main.css';

const Main = () => {
  return (
    <div className='main'>
      <Minute />
    </div>
  );
}

export default Main;
