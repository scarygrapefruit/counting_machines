import React, { useState } from 'react';

import DisplayCount from '../shared/display_count';
import incrementor from '../../utils/incrementor';

const MAX_DURATION = 60000; // amt milliseconds in one minute

const Minute = () => {
  const [count, setCount] = useState(1);
  incrementor(count, setCount, MAX_DURATION);

  return (
    <div>
      <DisplayCount count={ count } />
    </div>
  );
}

export default Minute;
