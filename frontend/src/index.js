import React from 'react';
import ReactDOM from 'react-dom';

import Main from './components/main';

import './css/reset.css';
import './css/index.css';

ReactDOM.render(
  <Main />,
  document.getElementById('root'),
);
