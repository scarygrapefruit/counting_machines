const incrementor = (count, setCount, maximum) => {
  const duration = randomInt(maximum);
  console.log('duration', duration);
  setTimeout(() => {
    setCount(count + 1)
  }, duration);
}

const randomInt = (maximum) => (
  Math.floor(Math.random() * (Math.floor(maximum) - 0 + 1) + 0)
);

export default incrementor;
